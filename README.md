# Numerical Read/Write Group
a.k.a `chmod 777 numerics`

Contact: Jeremy Thompson, Ren Stengel, Jed Brown

## Focus
The goal of this group is for members to learn about and be exposed to a range of methods for solving representative numerics problems (similar idea as [ParRes Kernels](https://github.com/ParRes/Kernels/), [the 100 Digit Challenge](https://en.wikipedia.org/wiki/Hundred-dollar,_Hundred-digit_Challenge_problems), and [Advent of Code](https://adventofcode.com/) but with group discussions). More specifically, we will focus on runtime, memory/hardware considerations, and applicable performance metrics when comparing different methods for the same problem (no free lunches). We can also explore the effect of various programming languages/frameworks on algorithm performance; however, experience with many different languages is not required to participate in this group.

## Format
This group will meet every other week in order to give group members time to work on and explore new methods for solving the problems. The beginning of every meeting will focus on group members sharing/discussing various solutions to the problem described in the previous meeting. The meeting will finish out with the next “presenter” proposing a new problem for members to work on until the next meeting.

## Schedule for Spring 2021

| Date       | Name   | Problem |
|------------|--------|-------------|
| 02/23/2021 | Jeremy | [Oribital Mechanics](https://gitlab.com/phypid/numerical-readwrite-group/-/tree/main/01-OrbitalMechanics) |
| 03/09/2021 | Zane   | [Trefethen's problem 9](https://gitlab.com/phypid/numerical-readwrite-group/-/tree/main/02-HighPrecisionIntegral)|
| 03/23/2021 |        | no meeting|
| 04/06/2021 | Ren    | [Fastest Laplacian solve to 6 digits](https://gitlab.com/phypid/numerical-readwrite-group/-/tree/main/03-FastestLaplacianToSixDigits)|
| 04/20/2021 |        | Wrap up for semester|

##### Problem ideas

- computing singular integrals
- solving a laplacian cheapest way to solve to 6 digits; could use FEM or pseudo-spectral methods
- hierarchical matrices
- [any of the 100 digit challenges](https://en.wikipedia.org/wiki/Hundred-dollar,_Hundred-digit_Challenge_problems)
- ray tracing
- any other problem you find interesting!

## Problem requirements
Problems can come from papers, textbooks/class assignments, current research, or personal projects. However, problems must be fully explained by the presenter during the intro meeting and in the problem’s README file (with references). Group members _are not be expected to prepare for each new problem_ (except for the presenter). Generally, problems should be focused on numerics techniques and problems.  Machine learning (ML) is out of the scope of this group but if someone has an interesting ML approach they are welcome to include it in the problem folder (see below).

Presenters should pick problems that are either small enough to quickly run on average computers or problems that can be shrunk down enough to run quickly (the presenter can provide additional, larger versions of the problem if they choose to). HPC access and experience is not required for this group.

## Organization of problems

Each problem should have its own stand-alone folder in this repository.

##### Data
Please upload any required data for your proposed problem. Include a small version of the problem so that members can have something that is easier to work with if they have limited time (or, for the sake of inclusion, limited access to fast computers with lots of memory). Larger versions of the problem are optional but encouraged (especially if the presenter is interested in a benchmark study). If necessary, please include a script for downloading and preprocessing data if you are unable to upload to the repository.

##### A (stand-alone) problem specific README file
###### Introduction
Describe the problem with references (diagrams are a bonus) and all required equations and assumptions to solve the problem.
Include _at least one baseline implementation_ (library or stand-alone, but shouldn't be too hard to install).

Define relevant metrics (accuracy, time, … proposed new ones?) as a basis for comparison.

Include any particular goal/performance aspect you want help improving (i.e. solve this problem faster) if applicable; however, group members don’t have to focus on this when designing their own solutions.

###### Requirements
Include instructions for installing and running the provided baseline implementation(s) and links to documentation for any required/recommended libraries.

Please also include a textual description of the data format. Include instructions for getting the data if you can't upload it.

###### Possible approaches
Include possible approaches for alternative implementations/etc. These could come from the presenter and/or from group discussion during the first meeting for the problem

This is not required but would be helpful for members with less experience.

###### Proposed solutions
A section for group members to document their implementations/modifications/metrics and results (textual and/or graphical - if applicable). Even if members don’t have time to actually implement a solution it is still valuable to share pseudocode/new approach descriptions with rationale and how the performance would be expected to change (not required). This will serve as an outline for in-meeting discussions as well as serve as a quick reference for future use and for members unable to attend that meeting or who joined the group later.

##### Implementations

Include complete code for running the baseline implementation(s).
Group members can add their own solutions to this folder via a merge request. Please give your files unique and useful names so that others' solutions don't get overwritten.

If a member wants to modify existing solutions it might be easiest to modify a copy of the code so that each implementation in the folder is stand alone (include installation instructions if different than the main problem)
