import jax
from jax import numpy as jnp
from scipy.integrate import quad
from jax.tree_util import Partial as partial
import numpy as np
jax.config.update('jax_enable_x64', True)
from solution import solution
import tanh_sinh


def bisect(f, a, b, tol=1.0e-10):
    """
    :param f: the 1-d function whose zero we want
    :param a: the lower bound on the interval
    :param b: the upper bound on the interval
    :param tol: the tolerance on the solution, defaults to 1.0e-10

    :return: the estimate of the ``x`` that solves ``f(x) = 0``
    """
    fa = f(a)
    fb = f(b)
    sfa = jnp.sign(fa)
    sfb = jnp.sign(fb)
    mid = a + (b - a) / 2
    fm = f(mid)
    sfm = jnp.sign(fm)
    assert sfa != sfb, "a and b must give positive and negative values, but both f(a) and f(b) have the same sign!"


    while abs(b - a) > 2 * tol:
        if sfm == sfa:
            a = mid
        else:
            b = mid
            
        mid = a + (b - a) / 2
        fm = f(mid)
        sfm = jnp.sign(fm)

    return mid


    
def integrand(x, alpha):
    return (2 + jnp.sin(10*alpha)) * (x ** alpha) * jnp.sin(alpha / (2 - x))


def asarray(f):
    def _scalar_f(*args):
        return np.array(f(*args))
    return _scalar_f

@asarray
@jax.jit
def grad_integrand_sin(x, alpha):
    return (x ** alpha) * ((jnp.sin(10*alpha) + 2) * jnp.log(x) + 10 * jnp.cos(10 * alpha)) * jnp.sin(alpha / (2 - x))

@asarray
@jax.jit
def grad_integrand_cos(x, alpha):
    return (x ** alpha) * (jnp.sin(10 * alpha) + 2) * jnp.cos(alpha / (2 - x)) / (2 - x)


def grad_integrand_sin_zeros(alpha, n):
    """
    returns the exact (to floating-point precision) first n zeros of 
    lambda x: grad_integrand_sin(x, alpha) as calculated by Mathematica
    """
    return [2 - alpha / (jnp.pi * (i+1)) for i in range(n)]

def grad_integrand_cos_zeros(alpha, n):
    """
    returns the exact (to floating-point precision) first n zeros of 
    lambda x: grad_integrand_cos(x, alpha) as calculated by Mathematica
    """
    return [2 - 2 * alpha / (2 * jnp.pi * (i+1) + jnp.pi) for i in range(n)]


def grad_integrand(x, alpha):
    return grad_integrand_sin(x, alpha) + grad_integrand_cos(x, alpha)

@jax.jit
def _crvz_1_sum(x):
    #returns sum of (-1)^k x_k for k in [0, n-1]
    n = len(x)
    d = (3 + jnp.sqrt(8)) ** n; d = (d + 1/d) / 2
    b = -1.0; c = -d; s = 0.0;
    def body_fun(k, val):
        (b, c, s, n, x) = val
        c = b - c
        s = s + c * x[k]
        b = (k + n) * (k - n) * b / ((k + 0.5) * (k + 1))
        return b, c, s, n, x

    b, c, s, n, x = jax.lax.fori_loop(0, n, body_fun, (b, c, s, n, x))
    return s / d
                                      
    

_acceleration_algorithms = {'crvz_1' : _crvz_1_sum}

def accelerated_sum(x, algorithm='crvz_1'):
    summer = _acceleration_algorithms[algorithm]
    return summer(x)


def scipy_integrator(alpha):
    return lambda f, a, b: quad(f, a, b, args=(alpha,), epsrel=1.0e-12, epsabs=1.0e-12, limit=100)[0]

def tanh_sinh_integrator(alpha):
    return lambda f, a, b: tanh_sinh.integrate(lambda x: f(x, alpha), a, b, 1.0e-12)[0]

def integrate_with_breaks(alpha, n, integrator=scipy_integrator, acceleration_algorithm='crvz_1'):
    sin_bpoints = [0] + grad_integrand_sin_zeros(alpha, n)
    cos_bpoints = [0] + grad_integrand_cos_zeros(alpha, n)
    integrator = integrator(alpha)
    sin_integrals = [integrator(grad_integrand_sin, sin_bpoints[i-1], sin_bpoints[i]) for i in range(1, n)]
    cos_integrals = [integrator(grad_integrand_cos, cos_bpoints[i-1], cos_bpoints[i]) for i in range(1, n)]
    #these are alternating series; the acceleration algorithms assume that the first term is positive,
    #so check if we need to negate the final result, and set all terms to be positive
    sin_sign = jnp.sign(sin_integrals[0])
    cos_sign = jnp.sign(cos_integrals[0])
    sin_integrals = list(map(abs, sin_integrals))
    cos_integrals = list(map(abs, cos_integrals))
    return sin_sign * accelerated_sum(jnp.array(np.array(sin_integrals)), acceleration_algorithm) + cos_sign * accelerated_sum(jnp.array(np.array(cos_integrals)), acceleration_algorithm)

def integrate_naive(alpha):
    #try this to see what happens! (hint: things won't go well)
    return quad(integrand_grad, 0.0, 2.0, args=(alpha,), epsrel=1.0e-12, epsabs=1.0e-12)[0]



if __name__ == '__main__':
    import time
    import cProfile, pstats
    print(f"Solving problem with true solution {solution():.10f}")
    a, b = 0.5, 1.0
    profiler = cProfile.Profile()
    start = time.time()
    estimate = bisect(lambda alpha: integrate_with_breaks(alpha, 12), a, b)
    stop = time.time()
    assert abs(estimate - solution()) < 1.0e-10
    print(f"Estimate is {estimate:.10f}, computed in {stop - start} seconds")

    start = time.time()
    estimate = bisect(lambda alpha: integrate_with_breaks(alpha, 12, integrator=tanh_sinh_integrator), a, b)
    stop = time.time()
    assert abs(estimate - solution()) < 1.0e-10
    print(f"Estimate is {estimate:.10f}, computed in {stop - start} seconds")
