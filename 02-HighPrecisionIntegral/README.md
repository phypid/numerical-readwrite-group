This problem is the solution of Trefethen's ninth Hundred-dollar, Hundred-digit problem, which is to determine the value of $`\alpha`$ that maximizes 
```math
I(\alpha) = \displaystyle\int\limits_0^2 x^{\alpha} (2 + \sin(10\alpha)) \sin(\frac{\alpha}{2 - x}) \ dx
```
to 10 significant digits. The answer is
```
0.7859336743
```
There is one primary feature of this integral that make this a challenging problem, which is that the $`\sin(\frac{\alpha}{2 - x})`$ term oscillates rapidly near $`x = 2`$ (as we will soon see, the derivative also has a singular term in the neighborhood of $`x = 2`$, but it turns out that adaptive Gaussian quadrature can handle this singularity just fine).


My baseline solution is written in JAX and SciPy in `jax/problem.py`, based on the solution presented in `http://www-m3.ma.tum.de/foswiki/pub/M3/Allgemeines/FolkmarBornemannPublications/short.pdf`, and using a series convergence acceleration algorithm from `https://mathscinet.ams.org/mathscinet-getitem?mr=2001m:11222`. It works as follows: first, analytically differentiate the integrand to get

```math
I'(\alpha) = \displaystyle\int\limits_0^2 x^{\alpha}(\sin(10\alpha) + 2) \left( \log(x) + 10\cos(10\alpha)\sin(\frac{\alpha}{2 - x}) + \dfrac{\cos(\frac{\alpha}{2 - x})}{2 - x}\right)\ dx
```
which can be split into a term proportional to $`\sin(\frac{\alpha}{2 - x})`$ and one proportional to $`\cos(\frac{\alpha}{2 - x})`$. These terms are the source of the primary difficulty with this integral, and their differing phases are the reason we need to analytically differentiate the integrand (as opposed to automatically differentiating it with JAX)---so we can split it up. We can use Mathematica to find the zeros of the two components of the integrand--- the one proportional to $`\sin(\frac{\alpha}{2 - x})`$ is zero (within our domain of integration) at $`x_{s,n} = 2 - \dfrac{\alpha}{n\pi}`$ for $`n\in\mathbb{Z}`$ with $`n\geq 1`$, while the term proportional to $`\cos(\frac{\alpha}{2 - x})`$ is zero at $`x_{c,n} = 2 - \dfrac{2\alpha}{2\pi n + \pi}`$ for $`n\in\mathbb{Z}`$ with $`n\geq 1`$. Defining $`x_{s,0} = x_{c,0} = 0`$, note that
```math
I'(\alpha) = \sum\limits_{i=1}^n \displaystyle\int\limits_{x_{s,i-1}}^{x_{s,i}}x^{\alpha}(\sin(10\alpha) + 2)(\log(x) + 10\cos(10\alpha)\sin(\frac{\alpha}{2 - x})) \ dx + \displaystyle\int\limits_{x_{s,i-1}}^{x_{s,i}}x^{\alpha}(\sin(10\alpha) + 2)\dfrac{\cos(\frac{\alpha}{2 - x})}{2 - x} \ dx
```
and both of those series are *alternating* series. This means that we can speed up the convergence of those series with the above-mentioned algorithm (Algorithm 1) from `https://mathscinet.ams.org/mathscinet-getitem?mr=2001m:11222`. We then take a look at the graph the authors of `http://www-m3.ma.tum.de/foswiki/pub/M3/Allgemeines/FolkmarBornemannPublications/short.pdf` presented of $`I(\alpha)`$ and `$I'(\alpha)$` and solve 
```math
I'(\alpha) = 0
```
via bisection on the interval `$[0.5,1]$`.

I found that we only need 12 terms of the series to get the required 10 digits of precision. To test out my solution, install all dependencies with
```
pip install jax jaxlib numpy scipy
```
and run it with
```
cd jax
python problem.py
```

Note that if you want to write your own solution that uses JAX, you will NEED to insert the following line of code before you do any calculations:
```
jax.config.update('jax_enable_x64', True)
```
or set the environment variable ``JAX_ENABLE_X64`` to ``True`` or else JAX will use 32-bit floats and you will lose precision (and the function calls from within the integrator, which is written in fortran, will be weirdly slow). I've done that in `problem.py` already, but it's something to be aware of when writing your own code if you chose to use JAX.

My first solution takes 2.9 seconds on my laptop (run `jax/problem.py` to time it on yours). The challenge is: can you do it faster? 

A hint: there probably isn't much time to be gained from switching languages. Feel free to do so if you want, but don't expect serious speedups from compiled languages, especially if you factor in compilation time, since most of the program is either in the fortran code in QUADPACK that `scipy` uses for integration or jitted code to evaluate the integrand and sum the series. A significantly faster solution will probably use a different approach to the problem; another solution presented in ``http://www-m3.ma.tum.de/foswiki/pub/M3/Allgemeines/FolkmarBornemannPublications/short.pdf` that relies on fast evaluation of the Meijer-G function seems promising.


EDIT: At Jed's suggestion, I tried `tanh-sinh` quadrature with the `tanh_sinh` library (`pip install tanh_sinh`), and that solves the problem in 1.66 seconds on my laptop.

