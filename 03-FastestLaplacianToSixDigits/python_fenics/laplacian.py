import numpy as np
import matplotlib.pyplot as plt
import csv
import time
from fenics import *
import pandas as pd

def laplacian_solve(mesh_refine, degree):

    mesh = UnitSquareMesh(mesh_refine, mesh_refine)
    V = FunctionSpace(mesh, 'P', degree)

    # Define boundary condition
    u_D = Constant(0.)
    sol = Expression('sin(pi*x[0])*sin(pi*x[1])', degree=degree+1)

    def boundary(x, on_boundary):
        return on_boundary

    bc = DirichletBC(V, u_D, boundary)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Expression('2*pi*pi*sin(pi*x[0])*sin(pi*x[1])', degree=degree) #Constant(-6.0)
    a = dot(grad(u), grad(v))*dx
    L = f*v*dx

    # Compute solution
    u = Function(V)
    solve(a == L, u, bc)

    times = [0., 0., 0.]
    error_L2 = [0., 0., 0.]
    error_max = [0., 0., 0.]

    for i in range(3):
        start = time.time()
        solve(a == L, u, bc)
        times[i] = time.time() - start


        # Plot solution and mesh
        #plot(u)
        #plot(mesh)

        # Save solution to file in VTK format
        # vtkfile = File('lap/solution.pvd')
        # vtkfile << u

        # Compute error in L2 norm
        error_L2[i] = errornorm(sol, u, 'L2')

        # Compute maximum error at vertices
        vertex_values_sol = sol.compute_vertex_values(mesh)
        vertex_values_u = u.compute_vertex_values(mesh)
        error_max[i] = np.linalg.norm(vertex_values_u - vertex_values_sol, np.inf)

        # Print errors
    #        print('error_L2  =', error_L2[i])
    #     print('error_max =', error_max[i])

    return np.mean(L2), np.mean(max_e), np.mean(times)

# main stuff.
mesh_refine = np.arange(10, 101, 10, np.int)
degree = [1, 2, 3, 4, 5]
deg_array = []
mesh_array = []
L2_array = []
max_array = []
time_array = []

filename = 'python_fenics/laplacian.csv'

for mesh in mesh_refine:
    for d in degree:
        L2, e_max, time_to_solve = laplacian_solve(mesh, d)

        deg_array.append(d)
        mesh_array.append(mesh)
        L2_array.append(L2)
        max_array.append(e_max)
        time_array.append(time_to_solve)

        print(d, mesh, L2, e_max, time_to_solve)
data = pd.DataFrame({'degree': deg_array,
                     'mesh refinement': mesh_array,
                     'L2 error': L2_array,
                     'max error': max_array,
                     'solve time (s)': time_array})
data.to_csv(filename)
