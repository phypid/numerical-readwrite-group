import numpy as np
import matplotlib.pyplot as plt
import csv
import time
import seaborn as sns
import pandas as pd

# seaborn cvs read in and formatting
directory = 'python_fenics/'
data=pd.read_csv(directory + 'laplacian.csv')
data.rename(columns={"solve time": "solve time (s)"}, inplace = True)

# time scatter plot
sns.relplot(x="mesh refinement", y="solve time (s)", hue = 'degree of solver', palette = 'colorblind', data=data)
plt.xscale('log')
plt.yscale('log')
plt.savefig(directory + 'plots/time_scatter.png', bbox_inches = 'tight')

# L2 error scatter plot
sns.relplot(x="mesh refinement", y="L2 error", hue = 'degree of solver', palette = 'colorblind', data=data)
plt.xscale('log')
plt.yscale('log')
plt.savefig(directory + 'plots/L2_scatter.png', bbox_inches = 'tight')

# Max error scatter plot
sns.relplot(x="mesh refinement", y="max error", hue = 'degree of solver', palette = 'colorblind', data=data)
plt.xscale('log')
plt.yscale('log')
plt.savefig(directory + 'plots/max_scatter.png', bbox_inches = 'tight')
