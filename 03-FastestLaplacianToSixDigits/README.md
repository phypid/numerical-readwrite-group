The goal of this week's problem is to solve a Laplacian as quick as possible to 6 digits (error less than 1e-6). The Laplacian of a function $`f`$ can be found by summing the second partial derivatives of $`f`$:
```math
\mathop{{}\bigtriangleup}\nolimits f = \sum_{i=1}^n \frac{\partial^2 f}{\partial x_i^2}\text{.}
```

This week we will be finding the Laplacian of $`u(x,y) = sin(\pi x)\cdot sin(\pi y)`$ which is equal to $`f(x,y) = 2\pi^2 u(x,y)`$.

The current implementation uses [FEniCS](https://fenicsproject.org/) to set up and solve the Laplacian with python. FEniCS for python can be installed with `conda create -n fenicsproject -c conda-forge fenics`.

To investigate the effect of polynomial degree and mesh refinement on the FeniCS solver, we tested degrees $`1,2,\dots,5`$ and unit meshes divided into $`10,20,\dots,100`$ cells in both $`x`$ and $`y`$ directions. Results are shown in the following plots (see table at the end for values).


![A test image](python_fenics/plots/time_scatter.png)
![A test image](python_fenics/plots/L2_scatter.png)
![A test image](python_fenics/plots/max_scatter.png)

### Jed's magic solution and results:

### Additional other methods?

Table of results:

<table class="tg">
<thead>
  <tr>
    <th class="tg-0lax">mesh</th>
    <th class="tg-0lax">degree</th>
    <th class="tg-0lax">L2 error</th>
    <th class="tg-0lax">max error</th>
    <th class="tg-0lax">Time (s)</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax" rowspan="5">10</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.02108923860509555</td>
    <td class="tg-0lax">0.024230720352252932</td>
    <td class="tg-0lax">0.002032756805419922</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">0.00028668859017717677</td>
    <td class="tg-0lax">6.351038049427771e-05</td>
    <td class="tg-0lax">0.004815975824991862</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">8.878835675441405e-06</td>
    <td class="tg-0lax">1.7988799929688493e-05</td>
    <td class="tg-0lax">0.009519656499226889</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">2.5559877345502767e-07</td>
    <td class="tg-0lax">1.3076702364001047e-07</td>
    <td class="tg-0lax">0.016794602076212566</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">6.1922041332920715e-09</td>
    <td class="tg-0lax">1.0952953904963536e-08</td>
    <td class="tg-0lax">0.0243837833404541</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">20</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.005379410300486611</td>
    <td class="tg-0lax">0.006140648734189957</td>
    <td class="tg-0lax">0.005335092544555664</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">3.539173694960777e-05</td>
    <td class="tg-0lax">4.292188072668041e-06</td>
    <td class="tg-0lax">0.018600622812906902</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">5.482831599847739e-07</td>
    <td class="tg-0lax">1.1881082176701252e-06</td>
    <td class="tg-0lax">0.04747128486633301</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">8.011181848952451e-09</td>
    <td class="tg-0lax">2.1300257391221322e-09</td>
    <td class="tg-0lax">0.10020581881205241</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">9.645336943969649e-11</td>
    <td class="tg-0lax">1.8387411732101455e-10</td>
    <td class="tg-0lax">0.1799586613972982</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">30</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.002399861986820203</td>
    <td class="tg-0lax">0.0027360493063236513</td>
    <td class="tg-0lax">0.009778022766113281</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">1.0461452278086842e-05</td>
    <td class="tg-0lax">8.645810904185647e-07</td>
    <td class="tg-0lax">0.04816667238871256</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">1.0782300639759165e-07</td>
    <td class="tg-0lax">2.3778726286963203e-07</td>
    <td class="tg-0lax">0.14284427960713705</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">1.0554406128074943e-09</td>
    <td class="tg-0lax">1.8884181371414677e-10</td>
    <td class="tg-0lax">0.3184026877085368</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">8.059875655948164e-12</td>
    <td class="tg-0lax">1.8600232465360023e-11</td>
    <td class="tg-0lax">0.6268755594889323</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">40</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.001351705600809157</td>
    <td class="tg-0lax">0.0015403824760785723</td>
    <td class="tg-0lax">0.017223676045735676</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">4.409737708349916e-06</td>
    <td class="tg-0lax">2.757531090127538e-07</td>
    <td class="tg-0lax">0.10710302988688152</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">3.403817908271532e-08</td>
    <td class="tg-0lax">7.56321370184998e-08</td>
    <td class="tg-0lax">0.3733462492624919</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">2.505160875991552e-10</td>
    <td class="tg-0lax">3.372484062846848e-11</td>
    <td class="tg-0lax">0.8016277949015299</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">3.011269114447355e-12</td>
    <td class="tg-0lax">8.597345058092287e-12</td>
    <td class="tg-0lax">1.7888344128926594</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">50</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.0008656208489818589</td>
    <td class="tg-0lax">0.0009862463098413432</td>
    <td class="tg-0lax">0.032308737436930336</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">2.256914065647714e-06</td>
    <td class="tg-0lax">1.1341172569730856e-07</td>
    <td class="tg-0lax">0.22263741493225098</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">1.3922551072075292e-08</td>
    <td class="tg-0lax">3.106086468498276e-08</td>
    <td class="tg-0lax">0.7092316150665283</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">8.219035093421922e-11</td>
    <td class="tg-0lax">8.83875351381791e-12</td>
    <td class="tg-0lax">1.7665379842122395</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">4.891395054408566e-12</td>
    <td class="tg-0lax">1.0593081967158469e-11</td>
    <td class="tg-0lax">4.650303602218628</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">60</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.0006013254338162117</td>
    <td class="tg-0lax">0.0006850447718577124</td>
    <td class="tg-0lax">0.048232714335123696</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">1.305811171480944e-06</td>
    <td class="tg-0lax">5.482430879069633e-08</td>
    <td class="tg-0lax">0.37021923065185547</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">6.7076634454532204e-09</td>
    <td class="tg-0lax">1.5002131061730983e-08</td>
    <td class="tg-0lax">1.414843002955119</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">3.338263939015671e-11</td>
    <td class="tg-0lax">7.0321526379757415e-12</td>
    <td class="tg-0lax">4.207506020863851</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">8.725060675000302e-12</td>
    <td class="tg-0lax">1.788869052887776e-11</td>
    <td class="tg-0lax">8.72441840171814</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">70</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.00044187869155580783</td>
    <td class="tg-0lax">0.0005033653113264425</td>
    <td class="tg-0lax">0.07553704579671223</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">8.222147881102135e-07</td>
    <td class="tg-0lax">2.9638128019524035e-08</td>
    <td class="tg-0lax">0.6010447343190511</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">3.617830635282925e-09</td>
    <td class="tg-0lax">8.105636395156629e-09</td>
    <td class="tg-0lax">2.484407583872477</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">1.6587623001750206e-11</td>
    <td class="tg-0lax">1.1709300196116601e-11</td>
    <td class="tg-0lax">7.336594978968303</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">1.0985670100744923e-11</td>
    <td class="tg-0lax">2.2116863895860206e-11</td>
    <td class="tg-0lax">18.049567937850952</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">80</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.0003383574067304289</td>
    <td class="tg-0lax">0.00038542243463679515</td>
    <td class="tg-0lax">0.12605659166971842</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">5.507746997564179e-07</td>
    <td class="tg-0lax">1.7391431410664418e-08</td>
    <td class="tg-0lax">0.9838907718658447</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">2.1190122159520764e-09</td>
    <td class="tg-0lax">4.754495903968106e-09</td>
    <td class="tg-0lax">4.384851058324178</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">1.132737245356637e-11</td>
    <td class="tg-0lax">1.5780265982812125e-11</td>
    <td class="tg-0lax">12.169519344965616</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">1.1675350071140736e-11</td>
    <td class="tg-0lax">2.3357316081273893e-11</td>
    <td class="tg-0lax">28.81088161468506</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">90</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.00026736795820284586</td>
    <td class="tg-0lax">0.0003045493316951031</td>
    <td class="tg-0lax">0.1668382485707601</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">3.8680501274435166e-07</td>
    <td class="tg-0lax">1.0865461599245466e-08</td>
    <td class="tg-0lax">1.5061736106872559</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">1.3220546328864496e-09</td>
    <td class="tg-0lax">2.969591163248797e-09</td>
    <td class="tg-0lax">7.006543159484863</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">1.12237190800571e-11</td>
    <td class="tg-0lax">2.0459522964699772e-11</td>
    <td class="tg-0lax">20.091341574986775</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">1.8048785583591045e-11</td>
    <td class="tg-0lax">3.634970102694979e-11</td>
    <td class="tg-0lax">45.10632197062174</td>
  </tr>
  <tr>
    <td class="tg-0lax" rowspan="5">100</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0.00021658188963196806</td>
    <td class="tg-0lax">0.00024669546897193584</td>
    <td class="tg-0lax">0.21986746788024902</td>
  </tr>
  <tr>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">2.8196960914754254e-07</td>
    <td class="tg-0lax">7.132747599156164e-09</td>
    <td class="tg-0lax">2.166910727818807</td>
  </tr>
  <tr>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">8.661034620622929e-10</td>
    <td class="tg-0lax">1.9490210511046574e-09</td>
    <td class="tg-0lax">10.199572324752808</td>
  </tr>
  <tr>
    <td class="tg-0lax">4</td>
    <td class="tg-0lax">1.3598325568414423e-11</td>
    <td class="tg-0lax">2.6575519562754835e-11</td>
    <td class="tg-0lax">29.14644678433736</td>
  </tr>
  <tr>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">1.951430071358242e-11</td>
    <td class="tg-0lax">3.896150069238047e-11</td>
    <td class="tg-0lax">67.32089408238728</td>
  </tr>
</tbody>
</table>
