## Julia implementation of solving Poisson on a square grid using Chebyshev collocation
# julia> load("poisson-fdm.jl")
# julia> test_error(7)
#
# Alternatively:
#
# $ julia poisson-fdm.jl
# L^2 error: 3.3101540684252654e-7
# Average time (µs): 32.359964999999995
# Average solve time (µs): 5.79946
using LinearAlgebra: eigen, inv, I, diagm, norm

function setup_fdm(m)
    """Set up fast diagonalization for a tensor product with degree m polynomials"""
    (D,xb) = cheb(m+1) # Chebyshev differentiation matrix and nodes
    D = 2*D; xb = 0.5 .+ 0.5*xb   # Remap to interval [0,1]
    D2 = -(D*D)[2:end-1,2:end-1] # Negative 1D Laplacian, with Dirichlet BCs
    xd = xb[2:end-1]             # Remove Dirichlet BCs
    # Compute action of inverse of L using sum factorization
    # See Lynch, Rice, and Thomas (1964)
    (Lambda, R) = eigen(D2)
    Diag = Lambda .+ Lambda'
    (xd, R, Diag)
end

function make_solution(x)
    sinx = sin.(pi*x)
    u_exact = sinx .* sinx'   # sin(pi*x)*sin(pi*y)
    b = 2 * pi^2 * u_exact
    (u_exact, b)
end

function solve(R, Diag, b)
    # L = kron(D2, I) + kron(I, D2)
    # Linv = kron(R,R) * diagm(vec(iDiag)) * kron(Rinv,Rinv)
    u = R * ((R \ b / R') ./ Diag) * R' # u = Linv * b  (with reshaping)
    u
end

function cheb(N)
    x = cos.(pi*(0:N)/N)
    c = ones(N+1); c[1] = 2; c[end] = 2; c[2:2:end] = -c[2:2:end]
    X = repeat(x,1,N+1)
    dX = X-X'
    D = (c * (1 ./ c')) ./ (dX + I)  # off-diagonal entries
    D -= diagm(vec(sum(D, dims=2)))              # diagonal entries
    (D, x)
end

function test_error(m)
    (x, R, Diag) = setup_fdm(m)
    (u_exact, b) = make_solution(x)
    u = solve(R, Diag, b)
    norm(u - u_exact)        # L2 norm of the vector
end

function main(m; count=1000)
    l2error = test_error(m)
    println("L^2 error: $l2error")
    t = @elapsed for i in 1:count
        test_error(7)
    end
    println("Average time (µs): ", 1e6*t/count)

    (x, R, Diag) = setup_fdm(m)
    (u_exact, b) = make_solution(x)
    t = @elapsed for i in 1:count
        solve(R, Diag, b)
    end
    println("Average solve time (µs): ", 1e6*t/count)
end

main(7)
