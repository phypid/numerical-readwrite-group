## Orbital Mechanics Problem

We investigate orbital mechanics, or the 'gravitational two-body problem'.

### Mathematical Background

Starting with first principals

```math
  \bold{F} = m \ddot{\bold{r}}
```

In this notation, **bold** indicates vector quantities while unbolded indicates scalar quantities, and temporal derivatives are given by overdots.

For the two-body problem, the forces on the orbiting body are given by

```math
  \bold{F} = - \frac{G m_1 m_2}{r^3}\bold{r}
```

where

* $`G`$ - Newton gravitational constant

* $`m_i`$ - Mass of body $`i`$

* $`\bold{r}`$ - Relative position of orbiting body

Applying this equation to both bodies and subtracting yields

```math
  \ddot{\bold{r}} = - \frac{G \left( m_1 + m_2 \right)}{r^3} \bold{r}
```

We can model the trajectory of the orbiting body by solving the system of ODEs

```math
  \begin{bmatrix}
    \dot{\bold{v}} \\
    \dot{\bold{r}}
  \end{bmatrix} =
  \begin{bmatrix}
    - \frac{G \left( m_1 + m_2 \right)}{r^3} \bold{r} \\
    \bold{v}
  \end{bmatrix}
```

where $`v = \dot{\bold{r}}`$.

The objective is to accurately model the trajectory of the orbiting body given an initial position and velocity.

### Performance Metrics

The following performance metrics can be used to measure the performance of the time integrator.

#### Orbital Energy

Orbital energy should be conserved

```math
  \epsilon = \epsilon_k + \epsilon_p = \frac{v^2}{2} - \frac{G \left( m_1 + m_2 \right)}{r}
```

#### Angular Momentum

Angular momentum should also be conserved

```math
  L = \lvert \bold{r} \times m \bold{v} \rvert
```

### Test Cases

All test cases are for bodies orbiting the Earth.

* $`m_1 = 5.9772 \cdot 10^{24} kg`$.

* $`G = 6.67430 \cdot 10^{-11} N m^2/kg^2`$ (NIST`s 2018 CODATA)

#### Test Case 1: Circular Orbit

Model the orbit of the International Space Station:

 * $`m_2 = 4.19725 \cdot 10^5 kg`$

 * $`\bold{r}_0 = \left[ 4073400, 0, 5431200 \right] m`$

 * $`\bold{v}_0 = \left[ 0, 7660, 0 \right] m/s`$

 * Model ten full orbits, $`926.8`$ minutes

Model the orbit of the Moon:

 * $`m_2 = 7.342 \cdot 10^22 kg`$

 * $`\bold{r}_0 = \left[ 230588670, 0, 307451560 \right] m`$

 * $`\bold{v}_0 = \left[ 0, 1022, 0 \right] m/s`$

 * Model ten full orbits, $`273.2`$ days

#### Test Case 2:  Elliptic Orbit

Model the orbit of Sputnik 1:

 * $`m_2 = 83.6 kg`$

 * $`\bold{r}_0 = \left[ 82692, 0, 198466 \right] m`$

 * $`\bold{v}_0 = \left[ 0, 8000, 0 \right] m/s`$

 * Model ten full orbits, $`962`$ minutes

Model the orbit of Molniya 1-1:

 * $`m_2 = 1600 kg`$

 * $`\bold{r}_0 = \left[ 15269231, 0, 36646154 \right] m`$

 * $`\bold{v}_0 = \left[ 0, 1500, 0 \right] m/s`$

 * Model ten full orbits, $`7180`$ minutes
