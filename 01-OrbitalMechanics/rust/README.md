## Rust example

This example only requires Rust and gnuplot.

To run

```
cargo run
```

To see the avaliable command line options

```
cargo run -- -h
```
