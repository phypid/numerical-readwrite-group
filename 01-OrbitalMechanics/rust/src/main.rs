use gnuplot::{Caption, Color, Figure, PointSize};
use structopt::StructOpt;

mod err;
mod opt;
mod solvers;
pub(crate) mod prelude {
    pub(crate) use crate::err;
    pub(crate) use crate::solvers;
}

// ----------------------------------------------------------------------------
// Main driver
// ----------------------------------------------------------------------------
fn main() -> err::Result<()> {
    let options = opt::Opt::from_args();
    orbital_mechanics(options)
}

// ----------------------------------------------------------------------------
// Orbital mechanics solver
// ----------------------------------------------------------------------------
fn orbital_mechanics(options: opt::Opt) -> err::Result<()> {
    // Process command line arguments
    let opt::Opt {
        r_0,
        v_0,
        m_1,
        m_2,
        g,
        t_f,
        d_t,
        time_stepper,
    } = options;
    // -- Get data from command line option sub-struct
    let r_0 = r_0.data;
    let v_0 = v_0.data;

    // Callback function for orbital mechanics
    let two_body = |state: &Vec<f64>| {
        // State: [v_x, v_y, v_z, r_x, r_y, r_z]
        let r_cubed = state
            .iter()
            .skip(3)
            .take(3)
            .fold(0.0, |sum, r| sum + r * r)
            .powf(1.5);
        let g_m_r = -g * (m_1 + m_2) / r_cubed;
        Ok(vec![
            g_m_r * state[3],
            g_m_r * state[4],
            g_m_r * state[5],
            state[0],
            state[1],
            state[2],
        ])
    };

    // Compute orbit
    let mut orbit = vec![vec![v_0[0], v_0[1], v_0[2], r_0[0], r_0[1], r_0[2]]];
    let num_steps = (t_f / d_t).ceil() as usize;
    for i in 0..num_steps {
        orbit.push(solvers::solve(time_stepper, &orbit[i], d_t, two_body)?);
    }

    // Diagnostic quantities
    let orbital_energy: Vec<f64> = orbit
        .iter()
        .map(|state| {
            // orbital energy = v²/2 - G(m₁ + m₂)/r
            let r = state
                .iter()
                .skip(3)
                .take(3)
                .fold(0.0, |sum, r| sum + r * r)
                .powf(0.5);
            let v_squared = state.iter().take(3).fold(0.0, |sum, v| sum + v * v);
            v_squared / 2.0 - g * (m_1 + m_2) / r
        })
        .collect();
    let angular_momentum: Vec<f64> = orbit
        .iter()
        .map(|state| {
            // angular momentum = | r̂ × m v̂ |
            ((state[1] * state[5] - state[2] * state[4]).powf(2.0)
                + (state[2] * state[3] - state[0] * state[5]).powf(2.0)
                + (state[0] * state[4] - state[1] * state[3]).powf(2.0))
            .powf(0.5)
        })
        .collect();

    // Plot results
    // -- Orbit
    let lat: Vec<f64> = orbit.iter().map(|state| state[4].atan2(state[3])).collect();
    let long: Vec<f64> = orbit
        .iter()
        .map(|state| ((state[3].powf(2.0) + state[4].powf(2.0)).powf(0.5) / state[5]).atan())
        .collect();
    let mut plot_lat_long = Figure::new();
    plot_lat_long.axes2d().points(
        &lat,
        &long,
        &[
            Caption("Orbital Trajectory"),
            Color("black"),
            PointSize(0.25),
        ],
    );
    plot_lat_long.show().unwrap();

    // -- Orbital Energy
    let time: Vec<f64> = (0..=num_steps).map(|i| d_t * i as f64).collect();
    let mut plot_energy = Figure::new();
    plot_energy.axes2d().points(
        &time,
        &orbital_energy,
        &[Caption("Orbital Energy"), Color("black"), PointSize(0.25)],
    );
    plot_energy.show().unwrap();

    // -- Angular Momentum
    let mut plot_energy = Figure::new();
    plot_energy.axes2d().points(
        &time,
        &angular_momentum,
        &[Caption("Angular Momentum"), Color("black"), PointSize(0.25)],
    );
    plot_energy.show().unwrap();

    Ok(())
}

// ----------------------------------------------------------------------------
