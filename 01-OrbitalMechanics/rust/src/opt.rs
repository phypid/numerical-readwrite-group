use crate::prelude::*;
use structopt::StructOpt;

// ----------------------------------------------------------------------------
// R3 vector
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy)]
pub(crate) struct R3Opt {
    pub(crate) data: [f64; 3],
}

impl std::str::FromStr for R3Opt {
    type Err = std::num::ParseFloatError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut input = s.splitn(3, ",");
        let mut data = [0.0; 3];
        for i in 0..3 {
            data[i] = input.next().unwrap().parse()?;
        }
        Ok(Self { data })
    }
}

// ----------------------------------------------------------------------------
// Command line arguments
// ----------------------------------------------------------------------------
#[derive(Debug, StructOpt)]
#[structopt(
    name = "Numerics Reading Group - Problem 01 Orbital Mechanics",
    about = "This example simulates the the gravitational two-body problem"
)]
pub(crate) struct Opt {
    /// Initial position, in m
    #[structopt(
        name = "Initial position",
        short = "r",
        long = "r_0",
        default_value = "4.073400E6,0.0,5.431200E6"
    )]
    pub(crate) r_0: R3Opt,
    /// Initial velocity, in m/s
    #[structopt(
        name = "Initial tangential velocity",
        short = "v",
        long = "v_0",
        default_value = "0.0,7.66E3,0.0"
    )]
    pub(crate) v_0: R3Opt,
    /// Mass of main body, in kg
    #[structopt(
        name = "Primary body mass",
        short = "M",
        long = "m_1",
        default_value = "5.9772E24"
    )]
    pub(crate) m_1: f64,
    /// Mass of orbiting body, in kg
    #[structopt(
        name = "Orbiting body mass",
        short = "m",
        long = "m_2",
        default_value = "4.19725E5"
    )]
    pub(crate) m_2: f64,
    /// Newton's gravitational constant, in N m^2/kg^2
    #[structopt(
        name = "Newton's gravitational constant",
        short = "G",
        default_value = "6.67430E-11"
    )]
    pub(crate) g: f64,
    /// Final time, in sec
    #[structopt(
        name = "Final time",
        short = "t",
        long = "t_f",
        default_value = "55610"
    )]
    pub(crate) t_f: f64,
    /// Step size, in sec
    #[structopt(
        name = "Time step size",
        short = "d",
        long = "dt",
        default_value = "0.1"
    )]
    pub(crate) d_t: f64,
    /// Time stepper
    #[structopt(
        name = "Time stepper",
        short = "s",
        long = "time_stepper",
        default_value = "ForwardEuler"
    )]
    pub(crate) time_stepper: solvers::Solvers,
}

// ----------------------------------------------------------------------------
