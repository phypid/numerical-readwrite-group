// -----------------------------------------------------------------------------
// Error message
// -----------------------------------------------------------------------------
use std::fmt;

pub(crate) type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub(crate) struct Error {
    pub message: String,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

// -----------------------------------------------------------------------------
