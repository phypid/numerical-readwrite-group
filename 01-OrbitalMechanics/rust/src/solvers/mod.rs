// ----------------------------------------------------------------------------
// Time steppers
// ----------------------------------------------------------------------------
use crate::prelude::*;

mod backward_euler;
mod forward_euler;
mod trapezoidal_rule;

// ----------------------------------------------------------------------------
// Solver options
// ----------------------------------------------------------------------------
#[derive(Debug, Copy, Clone)]
pub(crate) enum Solvers {
    ForwardEuler,
    BackwardEuler,
    TrapezoidalRule,
}

impl std::str::FromStr for Solvers {
    type Err = err::Error;
    fn from_str(s: &str) -> err::Result<Self> {
        match s {
            "ForwardEuler" => Ok(Self::ForwardEuler),
            "BackwardEuler" => Ok(Self::BackwardEuler),
            "TrapezoidalRule" => Ok(Self::TrapezoidalRule),
            _ => Err(err::Error {
                message: "option not found".to_string(),
            }),
        }
    }
}

// ----------------------------------------------------------------------------
// Solve
// ----------------------------------------------------------------------------
pub(crate) fn solve<F>(
    solver: Solvers,
    state: &Vec<f64>,
    dt: f64,
    callback: F,
) -> err::Result<Vec<f64>>
where
    F: Fn(&Vec<f64>) -> err::Result<Vec<f64>>,
{
    match solver {
        Solvers::ForwardEuler => forward_euler::solve(state, dt, callback),
        Solvers::BackwardEuler => backward_euler::solve(state, dt, callback),
        Solvers::TrapezoidalRule => trapezoidal_rule::solve(state, dt, callback),
    }
}

// ----------------------------------------------------------------------------
