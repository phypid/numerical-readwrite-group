// ----------------------------------------------------------------------------
// Forward Euler
// ----------------------------------------------------------------------------
use crate::prelude::*;

pub(crate) fn solve<F>(state: &Vec<f64>, dt: f64, callback: F) -> err::Result<Vec<f64>>
where
    F: Fn(&Vec<f64>) -> err::Result<Vec<f64>>,
{
    let d_state = callback(state)?;
    Ok(state
        .iter()
        .zip(d_state.iter())
        .map(|(x, dx_dt)| x + dt * dx_dt)
        .collect())
}

// ----------------------------------------------------------------------------
