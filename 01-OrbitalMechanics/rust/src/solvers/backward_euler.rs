// ----------------------------------------------------------------------------
// Backward Euler
// ----------------------------------------------------------------------------
use crate::prelude::*;

pub(crate) fn solve<F>(state: &Vec<f64>, dt: f64, callback: F) -> err::Result<Vec<f64>>
where
    F: Fn(&Vec<f64>) -> err::Result<Vec<f64>>,
{
    // Setup for fixed point iterations
    let max_iter = 250;
    let tol = 1e-14;
    let d_state = callback(state)?;
    let mut xn: Vec<f64> = state
        .iter()
        .zip(d_state.iter())
        .map(|(x, dx_dt)| x + dt * dx_dt)
        .collect();
    let mut error = 10.0 * tol;

    // Iterate to tolerance
    let mut iter = 0;
    while error > tol && iter < max_iter {
        let d_xn = callback(&xn)?;
        let xn1: Vec<f64> = state
            .iter()
            .zip(xn.iter().zip(d_xn.iter()))
            .map(|(x0, (xn, dxn_dt))| {
                let xeuler = x0 + dt * dxn_dt;
                xn - (xn - xeuler) / (1.0 - (xeuler - xn))
            })
            .collect();
        error = xn
            .iter()
            .zip(xn1.iter())
            .map(|(xn, xn1)| (xn - xn1).powf(2.0))
            .sum::<f64>()
            .powf(0.5);
        xn = xn1;
        iter += 1;
    }

    // Error cheching and return
    if iter == max_iter {
        Err(err::Error {
            message: "failure to converge".to_string(),
        })
    } else {
        Ok(xn)
    }
}

// ----------------------------------------------------------------------------
