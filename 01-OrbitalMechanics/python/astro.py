import jax
from jax import numpy as jnp
from integrators import integrate, crank_nicolson_step, backward_euler_step, symplectic_euler_step, build_explicit_rk_step, ButcherTableau, rk4_step, build_tableau, yoshida_4, rkfehlberg_solve
jax.config.update('jax_enable_x64', True)
import matplotlib.pyplot as plt
import numpy as np

G = 6.67430e-11
m_e = 5.9772e24
m_iss = 4.19725e5
m_moon = 7.342e22
MICROSECOND = 1.0e-6
MILLISECOND = 1.0e-3
SECOND = 1.0
MINUTE = 60.0
HOUR = 3600.0
DAY = 24 * HOUR
r_0iss = jnp.array([4073400.0, 0.0, 5431200.0])
v_0iss = jnp.array([0.0, 7660.0, 0.0])
r_0moon = jnp.array([230588670.0, 0.0, 307451560.0])
v_0moon = jnp.array([0.0, 1022.0, 0.0])


def plot_orbit(trajectory, plane_coords, title):
    trajx = [None] * len(trajectory)
    trajy = [None] * len(trajectory)
    for i in range(len(trajectory)):
        trajx[i] = trajectory[i, plane_coords[0]]
        trajy[i] = trajectory[i, plane_coords[1]]

    plt.figure()
    
    plt.scatter(trajx, trajy, s=0.5)
    plt.scatter(trajx[0], trajy[0], c='red', s=3, label='initial condition')
    plt.scatter(trajx[-1], trajy[-1], c='lime', s=3, label='final position')
    plt.legend()
    plt.title(title)
    plt.show()
            

def build_rhs(m_1, m_2):

    @jax.jit
    def rhs(x, t):
        r = x[0:3]
        v = x[3:6]
        vdot = -G * (m_1 + m_2) * r / jnp.linalg.norm(r) ** 3
        return jnp.concatenate([v, vdot])


    return rhs


def energy(state, m1, m2):
    r = state[0:3]
    v = state[3:6]
    return 0.5 * jnp.linalg.norm(v) ** 2 - G * (m1 + m2) / jnp.linalg.norm(r)


def angular_momentum(state, m):
    r = state[0:3]
    v = state[3:6]
    return jnp.abs(jnp.cross(r, m*v))
    

if __name__ == '__main__':
    
    moon_rhs = build_rhs(m_e, m_moon)
    moon_x0 = jnp.concatenate([r_0moon, v_0moon])
    moon_e0 = energy(moon_x0, m_e, m_moon)
    moon_l0 = angular_momentum(moon_x0, m_moon)

    iss_rhs = build_rhs(m_e, m_iss)
    iss_x0 = jnp.concatenate([r_0iss, v_0iss])
    iss_e0 = energy(iss_x0, m_e, m_iss)
    iss_l0 = angular_momentum(iss_x0, m_iss)

    iss_orbit_time = 926.8 * MINUTE
    iss_dt = 10 * SECOND

    iss_trajectory = integrate(iss_rhs, iss_x0, iss_dt, int(iss_orbit_time / iss_dt + 1), crank_nicolson_step).block_until_ready()
    print("x0 = ", iss_x0, " and xfinal is ", iss_trajectory[-1])
    iss_ef = energy(iss_trajectory[-1], m_e, m_iss)
    iss_lf = angular_momentum(iss_trajectory[-1], m_iss)
    print(f"ISS L0: {iss_l0} ISS LF: {iss_lf}")
    print(f"ISS energy before orbit: {iss_e0}. After 10 orbits: {iss_ef}. Discrepancy: {100 * (iss_ef - iss_e0) / jnp.abs(iss_e0)} percent")


    for stepper in [backward_euler_step, crank_nicolson_step,  symplectic_euler_step, rk4_step, yoshida_4]:
        moon_orbit_time = 273.2 * DAY
        moon_orbit_dt = 0.1 * HOUR
        moon_n = int(moon_orbit_time / moon_orbit_dt + 1)
        moon_traj = integrate(moon_rhs, moon_x0, moon_orbit_dt, moon_n, stepper).block_until_ready()
        moon_ef = energy(moon_traj[-1], m_moon, m_e)
        moon_lf = angular_momentum(moon_traj[-1], m_moon)
        print("With stepper ", stepper.__name__, ",")
        print("Moon L0: ", moon_l0, " moon LF: ", moon_lf)
        print("Moon E0: ", moon_e0, " moon EF: ", moon_ef, " Percent change: ", 100 * (moon_ef - moon_e0) / jnp.abs(moon_e0))

    #moon_dt_min = 1 * SECOND
    #moon_dt_max = 0.1 * HOUR
    #moon_truncation_tol = 1.0e-8
    #tf, moon_last_pos = rkfehlberg_solve(moon_rhs, moon_x0, 0., moon_orbit_time, moon_truncation_tol, moon_dt_min, moon_dt_max)
    #print("moon last: ", moon_last_pos)
    #moon_ef = energy(moon_last_pos, m_moon, m_e)
    #moon_lf = angular_momentum(moon_last_pos, m_moon)
    #print("With RK Fehlberg (MATLAB's ode45),")
    #print("Moon L0: ", moon_l0, " moon LF: ", moon_lf)
    #print("Moon E0: ", moon_e0, " moon EF: ", moon_ef, " Percent change: ", 100 * (moon_ef - moon_e0) / jnp.abs(moon_e0))

    for stepper in [yoshida_4, rk4_step]:#[backward_euler_step, crank_nicolson_step, symplectic_euler_step, rk4_step]:
        print("With stepper ", stepper.__name__ , ",")
        m_sputnik = 86.3
        sputnik_x0 = jnp.array([82692.0, 0.0, 198466.0, 0.0, 8000.0, 0.0])
        sputnik_e0 = energy(sputnik_x0, m_e, m_sputnik)
        sputnik_l0 = angular_momentum(sputnik_x0, m_sputnik)
        sputnik_orbit_time = 962 * MINUTE
        sputnik_dt = 3 * MILLISECOND
        sputnik_n = int(sputnik_orbit_time / sputnik_dt + 1)
        print(f"Integrating Sputnik orbit for {sputnik_n} timepoints. This may take a minute...")
        sputnik_rhs = build_rhs(m_e, m_sputnik)
        sputnik_traj = integrate(sputnik_rhs, sputnik_x0, sputnik_dt, sputnik_n,
                                 stepper).block_until_ready()

        sputnik_ef = energy(sputnik_traj[-1], m_e, m_sputnik)
        sputnik_lf = angular_momentum(sputnik_traj[-1], m_sputnik)
        print("Sputnik L0: ", sputnik_l0, " Sputnik Lf: ", sputnik_lf)
        print("Sputnik E0: ", sputnik_e0, " Sputnik Ef: ", sputnik_ef, " Percent change: ", 100 * (sputnik_ef - sputnik_e0) / jnp.abs(sputnik_e0))
        cut_traj = sputnik_traj[0:-1:1000, 0:3]
        print("Plotting orbit...")
        plot_orbit(np.array(cut_traj), (0, 1), "Sputnik orbit solved with " + stepper.__name__)

    sput_dt_min = 0.001 * MILLISECOND
    sput_dt_max = 10 * MINUTE
    sput_tf, sput_last_state, sput_traj = rkfehlberg_solve(sputnik_rhs, sputnik_x0, 0., sputnik_orbit_time, 1.0e-9, sput_dt_min, sput_dt_max, store_solution=True)
    sput_ef = energy(sput_traj[-1][1], m_sputnik, m_e)
    sput_lf = angular_momentum(sput_traj[-1][1], m_sputnik)
    print("With RK Fehlberg (MATLAB's ode45),")
    print("Sputnik L0: ", sputnik_l0, " sputnik LF: ", sput_lf)
    print("Sputnik E0: ", sputnik_e0, " sputnik EF: ", sput_ef, " Percent change: ", 100 * (sput_ef - sputnik_e0) / jnp.abs(sputnik_e0))
    

    m_mol = 1600.0
    x_0mol = jnp.array([15269231.0, 0.0, 36646154.0, 0.0, 1500.0, 0.0])
    mol_e0 = energy(x_0mol, m_mol, m_e)
    mol_rhs = build_rhs(m_e, m_mol)
    mol_orbit_time = 7180 * MINUTE
    mol_dt = 1 * SECOND
    mol_n = int(mol_orbit_time / mol_dt + 1)
    mol_l0 = angular_momentum(x_0mol, m_mol)

    mol_traj = integrate(mol_rhs, x_0mol, mol_dt, mol_n, symplectic_euler_step)
    mol_ef = energy(mol_traj[-1], m_e, m_mol)
    mol_lf = angular_momentum(mol_traj[-1], m_mol)
    print(" symplectic euler Molinya E0: ", mol_e0, " Molinya Ef: ", mol_ef, " percent change: ", 100 * (mol_ef - mol_e0) / jnp.abs(mol_e0))
    print("Molinya L0: ", mol_l0, " Molinya Lf: ", mol_lf)
    cut_traj = mol_traj[0:-1:100, 0:3]
    print("Plotting orbit...")
    plot_orbit(np.array(cut_traj), (0, 1), "Molinya orbit")

    mol_traj = integrate(mol_rhs, x_0mol, mol_dt, mol_n, rk4_step)
    mol_ef = energy(mol_traj[-1], m_e, m_mol)
    mol_lf = angular_momentum(mol_traj[-1], m_mol)
    print("Molinya E0: ", mol_e0, "RK4 Molinya Ef: ", mol_ef, " percent change: ", 100 * (mol_ef - mol_e0) / jnp.abs(mol_e0))
    print("Molinya L0: ", mol_l0, "RK4 Molinya Lf: ", mol_lf)
    cut_traj = mol_traj[0:-1:100, 0:3]
    print("Plotting orbit...")
    plot_orbit(np.array(cut_traj), (0, 1), "Molinya orbit RK4")


    mol_traj = integrate(mol_rhs, x_0mol, mol_dt, mol_n, yoshida_4)
    mol_ef = energy(mol_traj[-1], m_e, m_mol)
    mol_lf = angular_momentum(mol_traj[-1], m_mol)
    print("Molinya E0: ", mol_e0, "Yoshida Molinya Ef: ", mol_ef, " percent change: ", 100 * (mol_ef - mol_e0) / jnp.abs(mol_e0))
    print("Molinya L0: ", mol_l0, "Yoshida Molinya Lf: ", mol_lf)
    cut_traj = mol_traj[0:-1:100, 0:3]
    print("Plotting orbit...")
    plot_orbit(np.array(cut_traj), (0, 1), "Molinya orbit Yoshida")
