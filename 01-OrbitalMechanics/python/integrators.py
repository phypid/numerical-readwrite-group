import jax
from jax import numpy as jnp
from jax.tree_util import Partial as partial
import collections
import numpy as np
jax.config.update('jax_enable_x64', True)

def _newton_cond(val):
    x0, x1, rtol, i, max_iter = val
    return jax.lax.cond(jnp.linalg.norm(x1 - x0)/jnp.linalg.norm(x0) < rtol,
                                     lambda _ : False,
                                     lambda _ : True,
                                     None)

@partial(jax.jit, static_argnums=(0,))
def newton_solve(f, x0, rtol=1.0e-8, max_iter=1000, reverse_jac=False):
    jac_fn = jax.jacfwd(f) #function is R^n -> R^n
    def _newton_step(i, val):
        x0, x1, rtol = val
        y = f(x0)
        Jac = jac_fn(x0)
        return x0, x0 - jnp.linalg.solve(Jac, y), rtol

    #for now let's just run a fixed number of iterations since otherwise we'll have to do some weird jax stuff due to the early stopping condition
    x0, x1, rtol = jax.lax.fori_loop(0, max_iter, _newton_step, (x0, x0+100, rtol)) #while_loop(_newton_cond, _newton_step, (x0, x0 + 100, rtol, 0, max_iter))
    return x1


#@partial(jax.jit, static_argnums=(0,4))
def integrate(rhs, x0, dt, n, step):

    t = 0.0
    trajectory = jnp.zeros((n,) + x0.shape)
    @jax.jit
    def step_fun(i, state):
        x, t, trajectory = state
        xnew = step(rhs, x, t, dt)
        trajectory = jax.ops.index_update(trajectory, jax.ops.index[i, :], xnew)
        return xnew, t + dt, trajectory

    return jax.lax.fori_loop(0, n, step_fun, (x0, t, trajectory))[2]

@partial(jax.jit, static_argnums=(0,))
def rkfehlberg_k(f, y, t, dt):
    k1 = dt * f(t, y)
    k2 = dt * f(t + dt/4.0, y + k1/4.0)
    k3 = dt * f(t + 3*dt/8.0, y + 3*k1/32.0 + 9*k2/32.0)
    k4 = dt * f(t + 12*dt/13.0, y + 1932*k1/2197.0 - 7200*k2/2197.0 + 7296*k3/2197.0)
    k5 = dt * f(t + dt, y + 439*k1/216.0 -8*k2 + 3680*k3/513.0 - 845*k4/4101.0)
    k6 = dt * f(t + 0.5 * dt, y - 8*k1/27.0 + 2*k2 - 3544*k3/2565.0 + 1859*k4/4104.0 - 11*k5/40.0)

    return [k1, k2, k3, k4, k5, k6]

@jax.jit
def rkfehlberg_resid(ks, dt):
    return jnp.linalg.norm(ks[0]/360.0 - 128 * ks[2]/4275.0 - 2197*ks[3]/75240.0 + ks[4]/50.0 + 2 * ks[5]/55.0)

@jax.jit
def rkfehlberg_update(ks, y):
    return y + 25 * ks[0]/216.0 + 1408 * ks[2]/2565.0 + 2197 * ks[3]/4104.0 - 0.2 * ks[4]

@jax.jit
def rkfehlberg_delta(tol, resid):
    return 0.84 * (tol/resid) ** 0.25

def rkfehlberg_solve(f, x0, t0, tf, tol=1.0e-12, dt_min=1.0e-10, dt_max=100.0, store_solution=False):
    t = np.asarray(t0)
    x = x0
    dt = dt_max
    delta = 1.0
    steps=0
    txold = []

    while t < tf and dt > dt_min:
        ks = rkfehlberg_k(f, t, x, dt)
        r = rkfehlberg_resid(ks, dt)

        if np.asarray(r) <= tol:
            t += dt
            x = rkfehlberg_update(ks, x)
            if store_solution:
                txold.append((t,x))

        delta = rkfehlberg_delta(tol, r)

        if delta <= 0.1:
            dt = dt * 0.1
        elif delta >= 4.0:
            dt = dt * 4
        else:
            dt = dt * delta

        dt = np.asarray(min(dt, dt_max))

        if t + dt > tf:
            dt = tf - t
        elif dt < dt_min:
            raise ValueError(f"Error in rkfehlberg_solve: dt={dt}, which is less than dt_min={dt_min}.")

    if store_solution:
        return t, x, txold
    else:
        return t, x

@partial(jax.jit, static_argnums=(0,))
def backward_euler_step(rhs, x, t, dt, iters=30):

    @jax.jit
    def _F(x_t, x_tp1):
        return x_tp1 - dt * rhs(x_tp1, t + dt) - x_t

    return newton_solve(lambda val: _F(x, val), x, max_iter=iters)


@partial(jax.jit, static_argnums=(0,))
def crank_nicolson_step(rhs, x, t, dt, iters=30):

    @jax.jit
    def _F(x_t, x_tp1):
        #x_tp1 = x_t + 0.5 * dt * (f(x_tp1) + f(x_t))
        return x_tp1 - x_t - 0.5 * dt * (rhs(x_tp1, t + dt) + rhs(x_t, t))

    return newton_solve(lambda val: _F(x, val), x, max_iter=iters)

    
@partial(jax.jit, static_argnums=(0,))
def symplectic_euler_step(rhs, x, t, dt):
    r = x[0:3]
    v = x[3:6]
    vnext = v + rhs(x, t)[3:6] * dt
    rnext = r + vnext * dt
    return jnp.concatenate([rnext, vnext])


def unpack(x):
    return x[0:3], x[3:6]

def pack(r, v):
    return jnp.concatenate([r, v])

_w_0 = -pow(3, 1.0/3) / (2 - pow(2, 1.0/3))
_w_1 = 1.0 / (2 - pow(2,1.0/3))

@partial(jax.jit, static_argnums=(0,))
def yoshida_4(rhs, x, t, dt):
    c = [_w_1 / 2, (_w_0 + _w_1) / 2, (_w_0 + _w_1) / 2, _w_1/2]
    d = [_w_1, _w_0, _w_1]
    r0, v0 = unpack(x)
    r1 = r0 + c[0] * v0 * dt
    rprime1, vprime1 = unpack(rhs(pack(r1, v0), t))
    v1 = v0 + d[0] * vprime1 * dt
    
    r2 = r1 + c[1] * v1 * dt
    rprime2, vprime2 = unpack(rhs(pack(r2, v1), t))
    v2 = v1 + d[1] * vprime2 * dt

    r3 = r2 + c[2] * v2 * dt
    rprime3, vprime3 = unpack(rhs(pack(r3, v2), t))
    v3 = v2 + d[2] * vprime3 * dt

    r4 = r3 + c[3] * v3 * dt

    return pack(r4, v3)

    
ButcherTableau = collections.namedtuple('ButcherTableau', ['a','b','c'])

def mapped_array(f, arr):
    newarr =  np.zeros_like(arr, dtype=np.float64)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            newarr[i,j] = f(arr[i,j])

    return jnp.array(newarr)

def mapped_array_1d(f, arr):
    newarr = np.zeros_like(arr, dtype=np.float64)
    for i in range(arr.shape[0]):
        newarr[i] = f(arr[i])

    return jnp.array(newarr)
import matplotlib.pyplot as plt
def build_tableau(order: int, show_stability_region=False):
    import nodepy
    rk = nodepy.rk.extrap(order)
    if show_stability_region:
        rk.plot_stability_region()
        plt.show()
    S = rk.A.shape[0]-1
    b = mapped_array_1d(lambda x: x.evalf(), rk.b)
    A = mapped_array(lambda x: x.evalf(), rk.A)
    c = mapped_array_1d(lambda x: x.evalf(), rk.c)
    return ButcherTableau(a=A[:S,:S],b=b,c=c[1:])


def build_explicit_rk_step(tableau: ButcherTableau):

    if tableau.a.shape[0] != tableau.a.shape[1] or tableau.a.shape[0] != tableau.b.size -1 or tableau.b.size - 1 != tableau.c.size:
        raise ValueError(f"Butcher tableau has incorrect sizes! For s-stage RK, you need an (s-1)X(s-1) matrix a, an sX1 vector b, and an (s-1)X1 vector c! Shapes are {tableau.a.shape},{tableau.b.shape},{tableau.c.shape}")

    if not jnp.allclose(jnp.sum(tableau.b), 1.0, 1.0e-3):
        raise ValueError(f"b must sum to 1 for the method to be consistent! sum is {jnp.sum(tableau.b)}")

    def get_dxdt(s, a, ki):
        return jnp.dot(a[s,:s], ki[:s,:])

    S = tableau.b.size

    @partial(jax.jit, static_argnums=(0,))
    def explicit_rk_step(rhs, x, t, dt):
        #unroll the loop with jit because the loops are small
        ks = jnp.zeros((S,x.shape[0]))
        ks = jax.ops.index_update(ks, jax.ops.index[0,:], rhs(x, t))
        for i in range(1,S):
            tn = t + tableau.c[i-1] * dt
            dxdt = get_dxdt(i,tableau.a, ks)
            ks = jax.ops.index_update(ks, jax.ops.index[i,:], rhs(x + dt*dxdt, tn))
        return x + dt * jnp.einsum('i,ij -> j', tableau.b, ks)

    return explicit_rk_step




rk4_tableau = ButcherTableau(a=jnp.array([[0.5,0.0,0.0],[0.0,0.5,0.0],[0.0,0.0,1.0]]),
                             b=jnp.array([1.0/6,1.0/3,1.0/3,1.0/6]),
                             c=jnp.array([0.5, 0.5, 1.0]))

rk4_step = build_explicit_rk_step(rk4_tableau)

    
        
            
                       
            
        
